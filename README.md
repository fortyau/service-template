# Microservice Template
This template contains the architecture that should be utilized for any further microservice development. 
This document contains an overview of naming conventions and the folder structure.


## Naming Conventions
The naming structure of microservices which includes package naming, class naming, along with unit test naming.
All classes should follow the standard java naming conventions. For example:
-class name	should start with uppercase letter and be a noun e.g. String, Color, Button, System, Thread etc.
-interface name	should start with uppercase letter and be an adjective e.g. Runnable, Remote, ActionListener etc.
-method name	should start with lowercase letter and be a verb e.g. actionPerformed(), main(), print(), println() etc.
-variable name	should start with lowercase letter e.g. firstName, orderNumber etc.
-package name	should be in lowercase letter e.g. java, lang, sql, util etc.
-constants name	should be in uppercase letter. e.g. RED, YELLOW, MAX_PRIORITY etc.

## Directory Structure
Here is the listing of directories that the microservice should contain:
[src/main/java]
- net.cyc.service.[service_name] 
-- client
-- configuration
-- controller
------ serialize
-- dao
------ mapper
-- domain
-- service
------ batch
[src/main/resources]
- application.properties
