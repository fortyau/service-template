package net.cyc.service.template.controller;

import net.cyc.service.template.controller.TemplateServiceController;
import net.cyc.service.template.domain.*;
import net.cyc.service.template.domain.testonly.TestData;
import net.cyc.service.template.service.TemplateService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * this test should hit all of the endpoint possibilities of 
 * the service. this also includes the potential errors that
 * the service may throw back to a client.
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(TemplateServiceController.class)
public class CipServiceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TemplateService templateService;

    @Test
    public void test_service_endpoint() throws Exception {
        // TODO: follow up on this and other failure mode tests once we have further fleshed out our Exception handling.
        //       it would make more sense for this to return a 4xx
        mockMvc.perform(
                post("/url/to/endpoint")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        );
    }



    /*
    @Test
    public void test_cipRetrieveSingle_minimalDataIncludeNulls_expect200() throws Exception {
        final CipOrder expectedCipOrder = TestData.EXPECTED_CIP_ORDER_01;
        when(templateService.retrieveCipOrder(any(CipDataForRetrieve.class))).thenReturn(expectedCipOrder);
        mockMvc.perform(
                post("/cip/retrieve/single")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(TestData.CIP_RETRIEVE_SINGLE_REQUEST_MINIMAL_DATA_INCLUDE_NULLS)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_BATCH_ID, is(expectedCipOrder.getBatchId().intValue())))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_RECORD_ID, is(expectedCipOrder.getRecordId().intValue())))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_EMPLOYER_ID, is(expectedCipOrder.getEmployerId().intValue())))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_EMPLOYEE_ID, is(expectedCipOrder.getEmployeeId().intValue())))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_FIRST_NAME, is(expectedCipOrder.getFirstName())))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_LAST_NAME, is(expectedCipOrder.getLastName())))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_SSN, is(expectedCipOrder.getSsn())))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_STREET_ADDRESS_1, is(expectedCipOrder.getStreetAddress1())))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_STREET_ADDRESS_2, is(expectedCipOrder.getStreetAddress2())))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_CITY, is(expectedCipOrder.getCity())))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_STATE, is(State.getCode(expectedCipOrder.getState()))))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_ZIPCODE, is(expectedCipOrder.getZipcode())))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_PHONE, is(expectedCipOrder.getPhone())))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_EMAIL, is(expectedCipOrder.getEmail())))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_DATABASE_ID, is(expectedCipOrder.getDatabaseId().intValue())))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_CIP_STATUS, is(TemplateStatus.getCipStatus(expectedCipOrder.getCipStatus()))))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_CIP_STATUS_MODIFIED, is(Constants.DATE_TIME_FORMATTER.format(expectedCipOrder.getCipStatusModified()))))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_CIP_ORDER_CREATED, is(Constants.DATE_TIME_FORMATTER.format(expectedCipOrder.getCipOrderCreated()))))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_CIP_ORDER_MODIFIED, is(Constants.DATE_TIME_FORMATTER.format(expectedCipOrder.getCipOrderModified()))))
                .andExpect(jsonPath("$.cipOrder." + Constants.PROPERTY_NAME_REASONS, is(expectedCipOrder.getReasons())));
    }
    */
}
