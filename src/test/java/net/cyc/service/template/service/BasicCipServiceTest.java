package net.cyc.service.template.service;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import net.cyc.service.template.client.TemplateClient;
import net.cyc.service.template.dao.TemplateDao;
import net.cyc.service.template.service.BasicTemplateService;

@RunWith(SpringRunner.class)
public class BasicCipServiceTest {

    @MockBean
    private TemplateDao templateDao;


    // TODO: follow up with use of TemplateClient MockBean in unit tests
    @MockBean
    private TemplateClient templateClient;

    private BasicTemplateService basicTemplateService;

    @Before
    public void setup() {
        basicTemplateService = new BasicTemplateService(templateClient, templateDao);
    }

    @Test
    public void test1() {
    	assertTrue(true);
    }

}
