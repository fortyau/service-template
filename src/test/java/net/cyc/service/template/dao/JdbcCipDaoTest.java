package net.cyc.service.template.dao;

import net.cyc.service.template.dao.JdbcTemplateDao;
import net.cyc.service.template.domain.*;
import net.cyc.service.template.domain.testonly.TestData;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class JdbcCipDaoTest {

    private EmbeddedDatabase embeddedDatabase;
    private JdbcTemplateDao jdbcTemplateDao;
    private DefaultLobHandler defaultlobHandler = new DefaultLobHandler();

    @Before
    public void setup() {
        embeddedDatabase = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.HSQL)
                .addScript("create-embedded-hsql-db.sql")
                .addScript("insert-test-data.sql")
                .build();

        jdbcTemplateDao = new JdbcTemplateDao(new JdbcTemplate(embeddedDatabase), defaultlobHandler);
    }

    @After
    public void tearDown() {
        embeddedDatabase.shutdown();
    }

    public void test_some_dao_functionality() {
        // TODO: implement this test and others once the method is implemented in the src class.
    }




/*
    private void assertThatIsEqualToExceptStatusReasonsAndLexisNexisResponseId(CipOrder value, CipOrder expected) {
        assertThat(value).isNotNull();
        assertThat(expected).isNotNull();
        assertThat(value.getDatabaseId()).isEqualTo(expected.getDatabaseId());
        assertThat(value.getCipStatus()).isNotNull();
        assertThat(value.getCipStatusModified()).isNotNull();
        assertThat(value.getCipOrderCreated()).isNotNull();
        assertThat(value.getCipOrderModified()).isNotNull();
        assertThat(value.getEmployerId()).isEqualTo(expected.getEmployerId());
        assertThat(value.getEmployeeId()).isEqualTo(expected.getEmployeeId());
        assertThat(value.getFirstName()).isEqualTo(expected.getFirstName());
        assertThat(value.getLastName()).isEqualTo(expected.getLastName());
        assertThat(value.getSsn()).isEqualTo(expected.getSsn());
        assertThat(value.getDateOfBirth()).isEqualTo(expected.getDateOfBirth());
        assertThat(value.getStreetAddress1()).isEqualTo(expected.getStreetAddress1());
        assertThat(value.getStreetAddress2()).isEqualTo(expected.getStreetAddress2());
        assertThat(value.getCity()).isEqualTo(expected.getCity());
        assertThat(value.getState()).isEqualTo(expected.getState());
        assertThat(value.getZipcode()).isEqualTo(expected.getZipcode());
        assertThat(value.getPhone()).isEqualTo(expected.getPhone());
        assertThat(value.getEmail()).isEqualTo(expected.getEmail());
        assertThat(value.getBatchId()).isEqualTo(expected.getBatchId());
        assertThat(value.getRecordId()).isEqualTo(expected.getRecordId());
    }
*/
    
}
