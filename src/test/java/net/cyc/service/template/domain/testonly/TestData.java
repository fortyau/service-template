package net.cyc.service.template.domain.testonly;

import java.time.Instant;
import java.time.LocalDate;

import net.cyc.service.template.domain.Constants;
import net.cyc.service.template.domain.State;
import net.cyc.service.template.domain.TemplateStatus;

/**
 * Class for containing static data for tests so that it can be re-used.
 */
public class TestData {


    public static final Long NON_EXISTENT_EMPLOYER_ID = -100L;

    // data matching pre-inserted row from sql
    public static final Long DATABASE_ID_01 = 1001L;
    public static final TemplateStatus CIP_STATUS_01 = TemplateStatus.PASS;
    public static final Instant CIP_STATUS_MODIFIED_01 = Instant.parse("2017-01-02T03:04:05Z");
    public static final Instant CIP_ORDER_CREATED_01 = Instant.parse("2018-02-03T04:05:06Z");
    public static final Instant CIP_ORDER_MODIFIED_01 = Instant.parse("2019-03-04T05:06:07Z");
    public static final Long EMPLOYER_ID_01 = 1L;
    public static final Long EMPLOYEE_ID_01 = 1L;
    public static final String FIRST_NAME_01 = "alice";
    public static final String LAST_NAME_01 = "smith";
    public static final String SSN_01 = "111223333";
    public static final LocalDate DATE_OF_BIRTH_01 = LocalDate.of(1912, 1, 2);
    public static final String DATE_OF_BIRTH_01_STRING = DATE_OF_BIRTH_01.format(Constants.DATE_FORMATTER);
    public static final String STREET_ADDRESS_1_01 = "111 1st avenue";
    public static final String STREET_ADDRESS_2_01 = "apartment 1a";
    public static final String CITY_01 = "seattle";
    public static final State STATE_01 = State.WASHINGTON;
    public static final String STATE_01_STRING = STATE_01.getCode();
    public static final String ZIPCODE_01 = "12345";
    public static final String PHONE_01 = "1113335555";
    public static final String REASONS_01 = null;
    public static final String EMAIL_01 = "asmith@fake.com";
    public static final Long BATCH_ID_01 = 1L;
    public static final Long RECORD_ID_01 = 1L;

    // data matching pre-inserted row from sql
    public static final Long DATABASE_ID_02 = 1002L;
    public static final TemplateStatus CIP_STATUS_02 = TemplateStatus.UNPROCESSED;
    public static final Instant CIP_STATUS_MODIFIED_02 = Instant.parse("2020-02-03T04:05:06Z");
    public static final Instant CIP_ORDER_CREATED_02 = Instant.parse("2021-03-04T05:06:07Z");
    public static final Instant CIP_ORDER_MODIFIED_02 = Instant.parse("2022-04-05T06:07:08Z");
    public static final Long EMPLOYER_ID_02 = 1L;
    public static final Long EMPLOYEE_ID_02 = 2L;
    public static final String FIRST_NAME_02 = "bob";
    public static final String LAST_NAME_02 = "jones";
    public static final String SSN_02 = "222334444";
    public static final LocalDate DATE_OF_BIRTH_02 = LocalDate.of(1923, 3, 4);
    public static final String DATE_OF_BIRTH_02_STRING = DATE_OF_BIRTH_02.format(Constants.DATE_FORMATTER);
    public static final String STREET_ADDRESS_1_02 = "222 2nd street";
    public static final String STREET_ADDRESS_2_02 = "apt 2b";
    public static final String CITY_02 = "nashville";
    public static final State STATE_02 = State.TENNESSEE;
    public static final String STATE_02_STRING = STATE_02.getCode();
    public static final String ZIPCODE_02 = "23456";
    public static final String PHONE_02 = "3335557777";
    public static final String REASONS_02 = null;
    public static final String EMAIL_02 = "bjones@fake.com";
    public static final Long BATCH_ID_02 = 2L;
    public static final Long RECORD_ID_02 = 1L;



}
