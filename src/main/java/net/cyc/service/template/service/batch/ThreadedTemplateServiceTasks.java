package net.cyc.service.template.service.batch;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.cyc.service.template.service.TemplateService;

/**
 * Multi-threaded implementation of {@link TemplateServiceTasks}.
 */
public class ThreadedTemplateServiceTasks implements TemplateServiceTasks {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadedTemplateServiceTasks.class);



    /**
     * The {@link ExecutorService} that handles our threads and their execution.
     */
    private ExecutorService executorService;

    /**
     * The {@link TemplateService} used to process.
     */
    private TemplateService templateService;

    /**
     * Constructor.
     */
    public ThreadedTemplateServiceTasks(TemplateService templateService, Integer numberOfThreads) {
        this.templateService = Objects.requireNonNull(templateService, "templateService cannot be null.");
        executorService = Executors.newFixedThreadPool(numberOfThreads);

        // TODO: revisit this constructor once we setup the configuration for use with RefreshScope.
        //       as we will want to be able to configure things such as number of threads, timeouts, etc.
    }

    /**
     * {@inheritDoc}
     */
    public void callSomeMethod() {
        LOGGER.info("callSomeMethod(): Started");

        List<Object> objList = new ArrayList<Object>();
        // Create a List of Callables from the List of some Objects that you defined and then throw the ExecutorService
        // and its thread pool at it.
        final List<TemplateCallable> callableList = new ArrayList(10);
        for (Object obj : objList) {
            final TemplateCallable templateCallable = new TemplateCallable();
            callableList.add(templateCallable);
        }

        try {
            // TODO: revisit if we want to iterate over the List<Future> returned for any reason (ex: logging).
            executorService.invokeAll(callableList);
        } catch (InterruptedException interruptedException) {
            LOGGER.warn("Caught InterruptedException:", interruptedException);
        }

        LOGGER.info("callSomeMethod(): Finished from {} ", objList.size());
    }

    /**
     * @return templateService
     */
    private TemplateService getTemplateService() {
        return templateService;
    }

}
