package net.cyc.service.template.service.batch;

/**
 * Interface for methods handling various TemplateService tasks such as post-processing for batch and/or cleanup.
 */
public interface TemplateServiceTasks {

    /**
     * Continuously call some method within the service layer and have it do
     * whatever is needed.
     */
    public void callSomeMethod();

}
