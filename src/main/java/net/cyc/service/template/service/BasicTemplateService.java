package net.cyc.service.template.service;

import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.cyc.service.template.client.TemplateClient;
import net.cyc.service.template.dao.TemplateDao;
import net.cyc.service.template.domain.ServiceRequest;

/**
 * Basic {@link TemplateService} implementation.
 */
public class BasicTemplateService implements TemplateService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BasicTemplateService.class);

    /**
     * the {@link TemplateClient} used to communicate with the Notification service.
     */
    private TemplateClient templateClient;
    
    /**
     * The {@link TemplateDao} used for integrating with the persistence layer.
     */
    private TemplateDao templateDao;

    /**
     * Constructor.
     *
     * @throws NullPointerException if either of the given parameters is {@code null}.
     */
    public BasicTemplateService(TemplateClient templateClient, TemplateDao templateDao) {
    	this.templateClient = Objects.requireNonNull(templateClient, "templateClient cannot be null.");
        this.templateDao = Objects.requireNonNull(templateDao, "templateDao cannot be null.");
    }
    
    /**
     * Constructor.
     *
     * @throws NullPointerException if either of the given parameters is {@code null}.
     */
    public BasicTemplateService(TemplateDao templateDao) {
    	this.templateDao = Objects.requireNonNull(templateDao, "templateDao cannot be null.");
    }


    
    @Override
    public List<Object> someMethod(ServiceRequest serviceRequest) {
    	// TODO Auto-generated method stub
    	return null;
    }
    /**
     * @return templateDao
     */
    private TemplateDao getTemplateDao() {
        return templateDao;
    }
}
