package net.cyc.service.template.service.batch;

import java.util.concurrent.Callable;

import net.cyc.service.template.domain.ServiceRequest;
import net.cyc.service.template.service.TemplateService;

/**
 * {@link Callable} implementation for calling {@link TemplateService#processCipOrder(CipOrder)} on a given {@link CipOrder}.
 */
class TemplateCallable implements Callable<Boolean> {

    /**
     * The {@link TemplateService} to use to process the {@link CipOrder}.
     */
    private TemplateService templateService;

    private ServiceRequest serviceRequest;
    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean call() throws Exception {       
        return true;
    }

    /**
     * @return templateService
     */
    private TemplateService getTemplateService() {
        return templateService;
    }
    
    private ServiceRequest getServiceRequest() {
    	return serviceRequest;
    }


}
