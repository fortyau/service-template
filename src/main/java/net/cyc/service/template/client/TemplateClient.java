/**
 * 
 */
package net.cyc.service.template.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import net.cyc.service.template.domain.ServiceRequest;




public class TemplateClient {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(TemplateClient.class);
    
    private final String url;

    /**
     * get the url and endpoint for buidling a complete URL to
     * call the required service
     * @param serviceUrl
     * @param serviceEndpoint
     */
    public TemplateClient(String serviceUrl, String serviceEndpoint) {
		url = serviceUrl + serviceEndpoint;
	}

    public void callSerivce() {
        // TODO this is completely stubbed out code. 
        try {
            RestTemplate restTemplate = new RestTemplate();
            ServiceRequest serviceRequest = new ServiceRequest();            
            ResponseEntity<?> response = restTemplate.postForEntity(url, serviceRequest, String.class);
            if(response.getStatusCode() == HttpStatus.OK) {
            	LOGGER.info(response.getBody().toString());
                // TODO handle a successful call
            }
            // TODO handle non-200 response(s)
        } catch(Exception exception) {
            LOGGER.error("Error calling the client service: ", exception);
            // TODO revisit how we want to handle this. Would like to make sure we follow-up with sending the notification
            //      however, we don't want to leave a client with the impression that the overall process failed, so not rethrowing
        }
    }
}
