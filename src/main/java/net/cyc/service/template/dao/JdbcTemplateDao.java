package net.cyc.service.template.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.lob.LobHandler;

import net.cyc.service.template.domain.Constants;

// TODO: Revisit how we want to handle case (upper/lower) since we'll be performing searches.
//       This will be dictated by the [lack of] support for case insensitive comparisons in the persistence layer.
/**
 * this is the Dao layer where all of the data access logic will reside. it will implement the interface for DAO 
 * layer objects.
 * 
 * {@link TemplateDao} implementation that leverages JDBC to integrate with the persistence layer.
 */
public class JdbcTemplateDao implements TemplateDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcTemplateDao.class);

    private static final String SQL_SELECT_STMT = "SELECT * fROM TEMPLATE";
    /**
     * {@link JdbcTemplate} used to communicate with the persistence layer.
     */
    private final JdbcTemplate jdbcTemplate;

    /**
     * {@link LobHandler} for handling the use of CLOBs by this class.
     */
    private final LobHandler lobHandler;


    /**
     * Constructor.
     *
     * @throws NullPointerException if any of the given parameters is {@code null}.
     */
    public JdbcTemplateDao(JdbcTemplate jdbcTemplate, LobHandler lobHandler) {
        this.jdbcTemplate = Objects.requireNonNull(jdbcTemplate, "jdbcTemplate cannot be null.");
        this.lobHandler = Objects.requireNonNull(lobHandler, "lobHandler cannot be null.");

    }


    /**
     * {@inheritDoc}
     */
    public boolean updateTable(String value1, String value2) {
        Validate.isTrue(value1 != null);
        Validate.isTrue(value2 != null);

        final QueryAndArgs queryAndArgs = generateQueryAndArgs();

        final int rowsModified = getJdbcTemplate().update(queryAndArgs.getQuery(),
                queryAndArgs.getArgs().toArray());

        if (rowsModified != 1) {
            LOGGER.info("table was not updated");
            return false;
        }

        return true;
    }




    /**
     * Generate an update SQL statement and {@link List} of args for it 
     *
     * @return a non-{@code null} {@link QueryAndArgs}.
     */
    private QueryAndArgs generateQueryAndArgs() {

        final StringBuilder query = new StringBuilder();
        final List<Object> args = new ArrayList();

        query.append(Constants.COLUMN_NAME + " = ? ");
        args.add("test");//cipStatus.getCode();


        final QueryAndArgs queryAndArgs = new QueryAndArgs(query.toString(), args);
        return queryAndArgs;
    }



    private JdbcTemplate getJdbcTemplate() {

		return this.getJdbcTemplate();
	}


    /**
     * Private class for encapsulating a SQL query {@link String} and an associated {@link List} of args.
     */
    private class QueryAndArgs {
        /**
         * The SQL query.
         */
        private String query;

        /**
         * The {@link List} of args for the SQL query.
         */
        private List<Object> args;

        /**
         * Constructor.
         *
         * @throws IllegalArgumentException if any of the parameters fails validation.
         */
        QueryAndArgs(String query, List<Object> args) {
            Validate.isTrue(StringUtils.isNotBlank(query));
            Validate.isTrue(args != null);

            this.query = query;
            this.args = args;
        }

        /**
         * @return query
         */
        String getQuery() {
            return query;
        }

        /**
         * @return args
         */
        List<Object> getArgs() {
            return args;
        }
    }

}
