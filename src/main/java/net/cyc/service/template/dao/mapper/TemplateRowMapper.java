package net.cyc.service.template.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

/**
 * {@link RowMapper} implementation for {@link Object}s.
 * note: {@link Object} will be replaced with whatever class
 * needs to be mapped.
 */
public class TemplateRowMapper implements RowMapper<Object> {

    /**
     * {@link TemplateRowMapper} singleton.
     */
    private static final TemplateRowMapper templateRowMapper = new TemplateRowMapper();

    /**
     * Private constructor per singleton.
     */
    private TemplateRowMapper() {
    }

    /**
     * Return the {@link TemplateRowMapper} singleton.
     *
     * @return a non-{@code null} {@link TemplateRowMapper}.
     */
    public static TemplateRowMapper getInstance() {
        return templateRowMapper;
    }

    /**
     * get a listing of values from the resultSet and pass
     * them to the required object's constructor 
     * {@inheritDoc}
     */
    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {

        final Long employerId = resultSet.getLong("employer_id");
        final Long employeeId = resultSet.getLong("employee_id");
        final String firstName = resultSet.getString("first_name");
        final String lastName = resultSet.getString("last_name");

        final Object obj = new Object();
//        final Object obj = new Object(employerId,
//                employeeId,
//                firstName,
//                lastName);

        return obj;
    }

}
