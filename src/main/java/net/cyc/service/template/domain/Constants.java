package net.cyc.service.template.domain;

import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class Constants {




    public static final String COLUMN_NAME = "column_name";

    public static final String DELIMITER_REASONS = ";";

    public static final String FORMAT_DATE = "yyyy-MM-dd";
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(FORMAT_DATE).withZone(ZoneOffset.UTC);
    public static final String FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(FORMAT_DATE_TIME).withZone(ZoneOffset.UTC);	
}
