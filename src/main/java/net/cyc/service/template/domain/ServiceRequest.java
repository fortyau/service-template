/**
 * 
 */
package net.cyc.service.template.domain;

import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

// TODO: revisit this class and clean it up a bit once we have user, employer, and employee services setup.
/**
 * @author Jeff Corbett
 * This is the class that will be populated from the clients
 * in order to send data into a service request. this can
 * be email information, user information, etc.
 */
public class ServiceRequest {

	private String value;

	
	/**
	 * 
	 */
	public ServiceRequest()
	{	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	@Override
	public String toString() {
		return "ServiceRequest [value=" + value + "]";
	}

	


}

