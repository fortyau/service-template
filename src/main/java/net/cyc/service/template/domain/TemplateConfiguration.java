package net.cyc.service.template.domain;

import org.springframework.core.io.Resource;

public class TemplateConfiguration {

    private Resource keyStore;
    private String keyStorePassword;
    private String soapEndpoint;
    private String accountName;
    private String ruleset;

    public TemplateConfiguration(Resource keyStore, String keyStorePassword, String soapEndpoint, String accountName, String ruleset) {
        this.keyStore = keyStore;
        this.keyStorePassword = keyStorePassword;
        this.soapEndpoint = soapEndpoint;
        this.accountName = accountName;
        this.ruleset = ruleset;
    }

    public Resource getKeyStore() {
        return keyStore;
    }

    public String getKeyStorePassword() {
        return keyStorePassword;
    }

    public String getSoapEndpoint() {
        return soapEndpoint;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getRuleset() {
        return ruleset;
    }
}
