package net.cyc.service.template.domain;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TemplateStatus {

    UNPROCESSED("Unprocessed", "UNP"),
    PROCESSING("Processing", "PRO"),
    PENDING("Pending", "PEN"),
    PASS("Pass", "PAS"),
    DECLINED("Declined", "DEC");

    /**
     * String representation; decoupling of value that may displayed from the actual enum value used for logic.
     * Also leveraged as the JSON representation.
     */
    private final String templateStatus;

    /**
     * Shorter {@link String} representation for more smaller persistence footprint.
     */
    private final String code;

    /**
     * Constructor.
     */
    private TemplateStatus(String templateStatus, String code) {
        this.templateStatus = templateStatus;
        this.code = code;
    }

    /**
     * Get an instance of {@link TemplateStatus} matching the given {@link String} {@code value}.
     *
     * @param value the value to use to get an instance of {@link TemplateStatus}.
     * @return {@code null} or a {@link TemplateStatus}.
     * @throws IllegalArgumentException if the given {@link String} {@code value} does not match any {@link TemplateStatus}.
     */
    @JsonCreator
    public static TemplateStatus getInstance(String value) {
        if (value == null) {
            return null;
        }

        // first try to match on code
        for (TemplateStatus templateStatus : TemplateStatus.values()) {
            if(StringUtils.equalsIgnoreCase(templateStatus.getCode(), value)) {
                return templateStatus;
            }
        }

        // second chance try to match on cipStatus
        for (TemplateStatus templateStatus : TemplateStatus.values()) {
            if(StringUtils.equalsIgnoreCase(templateStatus.toString(), value)) {
                return templateStatus;
            }
        }

        // No matches so unknown value.
        final String errorMessage = String.format("Error message", value);
        throw new IllegalArgumentException(errorMessage);
    }

    /**
     * {@code null}-safe method for retrieving the {@code cipStatus} for a {@link TemplateStatus}.
     * @param templateStatus the {@link TemplateStatus} to retrieve a {@code cipStatus} from.
     * @return {@code null} or a {@code String}.
     */
    public static String getCipStatus(TemplateStatus templateStatus) {
        if (templateStatus == null) {
            return null;
        }

        return templateStatus.getTemplateStatus();
    }

    /**
     * {@code null}-safe method for retrieving the {@code code} for a {@link TemplateStatus}.
     * @param templateStatus the {@link TemplateStatus} to retrieve a {@code code} from.
     * @return {@code null} or a {@code String}.
     */
    public static String getCode(TemplateStatus templateStatus) {
        if (templateStatus == null) {
            return null;
        }

        return templateStatus.getCode();
    }

    /**
     * @return cipStatus
     */
    @JsonValue
    public String getTemplateStatus() {
        return templateStatus;
    }

    /**
     * @return code
     */
    public String getCode() {
        return code;
    }

}
