package net.cyc.service.template.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * U.S. State (or territory)
 */
public enum State {

    // 50 States plus Washington D.C.
    ALABAMA("Alabama", "AL"),
    ALASKA("Alaska", "AK"),
    ARIZONA("Arizona", "AZ"),
    ARKANSAS("Arkansas", "AR"),
    CALIFORNIA("California", "CA"),
    COLORADO("Colorado", "CO"),
    CONNECTICUT("Connecticut", "CT"),
    DELAWARE("Delaware", "DE"),
    DISTRICT_OF_COLUMBIA("District of Columbia", "DC"),
    FLORIDA("Florida", "FL"),
    GEORGIA("Georgia", "GA"),
    HAWAII("Hawaii", "HI"),
    IDAHO("Idaho", "ID"),
    ILLINOIS("Illinois", "IL"),
    INDIANA("Indiana", "IN"),
    IOWA("Iowa", "IA"),
    KANSAS("Kansas", "KS"),
    KENTUCKY("Kentucky", "KY"),
    LOUISIANA("Louisiana", "LA"),
    MAINE("Maine", "ME"),
    MARYLAND("Maryland", "MD"),
    MASSACHUSETTS("Massachusetts", "MA"),
    MICHIGAN("Michigan", "MI"),
    MINNESOTA("Minnesota", "MN"),
    MISSISSIPPI("Mississippi", "MS"),
    MISSOURI("Missouri", "MO"),
    MONTANA("Montana", "MT"),
    NEBRASKA("Nebraska", "NE"),
    NEVADA("Nevada", "NV"),
    NEW_HAMPSHIRE("New Hampshire", "NH"),
    NEW_JERSEY("New Jersey", "NJ"),
    NEW_MEXICO("New Mexico", "NM"),
    NEW_YORK("New York", "NY"),
    NORTH_CAROLINA("North Carolina", "NC"),
    NORTH_DAKOTA("North Dakota", "ND"),
    OHIO("Ohio", "OH"),
    OKLAHOMA("Oklahoma", "OK"),
    OREGON("Oregon", "OR"),
    PENNSYLVANIA("Pennsylvania", "PA"),
    RHODE_ISLAND("Rhode Island", "RI"),
    SOUTH_CAROLINA("South Carolina", "SC"),
    SOUTH_DAKOTA("South Dakota", "SD"),
    TENNESSEE("Tennessee", "TN"),
    TEXAS("Texas", "TX"),
    UTAH("Utah", "UT"),
    VERMONT("Vermont", "VT"),
    VIRGINIA("Virginia", "VA"),
    WASHINGTON("Washington", "WA"),
    WEST_VIRGINIA("West Virginia", "WV"),
    WISCONSIN("Wisconsin", "WI"),
    WYOMING("Wyoming", "WY"),

    // Non-states (Territories, etc.)
    AMERICAN_SAMOA("American Samoa", "AS"),
    FEDERATED_STATES_OF_MICRONESIA("Federated States of Micronesia", "FM"),
    GUAM("Guam", "GU"),
    MARSHALL_ISLANDS("Marshall Islands", "MH"),
    NORTHERN_MARIANA_ISLANDS("Northern Mariana Islands", "MP"),
    PALAU("Palau", "PW"),
    PUERTO_RICO("Puerto Rico", "PR"),
    VIRGIN_ISLANDS("Virgin Islands", "VI");

    /**
     * String representation; decoupling of value that may displayed from the actual enum value used for logic.
     */
    private final String state;

    /**
     * Shorter {@link String} representation for more smaller persistence footprint.
     * Also leveraged as the JSON representation.
     */
    private final String code;

    /**
     * Constructor.
     */
    private State(String state, String code) {
        this.state = state;
        this.code = code;
    }

    /**
     * Get an instance of {@link State} matching the given {@link String} {@code value}.
     *
     * @param value the value to use to get an instance of {@link State}.
     * @return {@code null} or a {@link State}.
     * @throws IllegalArgumentException if the given {@link String} {@code value} does not match any {@link State}.
     */
    @JsonCreator
    public static State getInstance(String value) {
        if (value == null) {
            return null;
        }

        // first try to match on code
        for (State state : State.values()) {
            if (StringUtils.equalsIgnoreCase(state.getCode(), value)) {
                return state;
            }
        }

        // second chance try to match on state
        for (State state : State.values()) {
            if (StringUtils.equalsIgnoreCase(state.getState(), value)) {
                return state;
            }
        }

        // No matches so unknown value.
        final String errorMessage = String.format("error message", value);
        throw new IllegalArgumentException(errorMessage);
    }

    /**
     * {@code null}-safe method for retrieving the {@code state} for a {@link State}.
     * @param state the {@link State} to get the {@code state} from.
     * @return {@code null} or a {@code String}.
     */
    public static String getState(State state) {
        if (state == null) {
            return null;
        }

        return state.getState();
    }

    /**
     * {@code null}-safe method for retrieving the {@code code} for a {@link State}.
     * @param state the {@link State} to get the {@code code} from.
     * @return {@code null} or a {@code String}.
     */
    public static String getCode(State state) {
        if (state == null) {
            return null;
        }

        return state.getCode();
    }

    /**
     * @return state
     */
    public String getState() {
        return state;
    }

    /**
     * @return code
     */
    @JsonValue
    public String getCode() {
        return code;
    }
}
