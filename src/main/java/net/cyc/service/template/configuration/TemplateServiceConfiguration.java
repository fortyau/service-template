package net.cyc.service.template.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.cyc.service.template.client.TemplateClient;
import net.cyc.service.template.dao.TemplateDao;
import net.cyc.service.template.service.BasicTemplateService;
import net.cyc.service.template.service.TemplateService;
import net.cyc.service.template.service.batch.TemplateServiceTasks;
import net.cyc.service.template.service.batch.ThreadedTemplateServiceTasks;

@Configuration
public class TemplateServiceConfiguration {

    // TODO: revisit numberOfThreads when we beginning to leverage RefreshScope for this service.
    /**
     * The number of threads to construct {@link ThreadedTemplateServiceTasks} with.
     */
    private final Integer numberOfThreads = 5;

    @Autowired
    @Bean("basicService")
    public TemplateService basicService(TemplateClient templateClient, TemplateDao templateDao) {
        final TemplateService templateService = new BasicTemplateService(templateDao);
        return templateService;
    }

    @Autowired
    @Bean
    public TemplateServiceTasks threadedServiceTasks(TemplateService templateService) {
        TemplateServiceTasks templateServiceTasks = new ThreadedTemplateServiceTasks(templateService, numberOfThreads);
        return templateServiceTasks;
    }

}
