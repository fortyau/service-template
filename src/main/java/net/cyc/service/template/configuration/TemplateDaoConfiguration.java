package net.cyc.service.template.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;

import net.cyc.service.template.dao.TemplateDao;
import net.cyc.service.template.dao.JdbcTemplateDao;

import javax.sql.DataSource;

@Configuration
public class TemplateDaoConfiguration {

    @Value("${dao.jdbc-cip-dao.days-to-expire}")
    private int daysToExpire;

    @Value("${dao.datasource.driver-class-name}")
    private String driverClassName;

    @Value("${dao.datasource.url}")
    private String url;

    @Value("${dao.datasource.username}")
    private String username;

    @Value("${dao.datasource.password}")
    private String password;

    @Bean
    public DataSource awsPostgresqlDataSource() {
        final DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(driverClassName);
        dataSourceBuilder.username(username);
        dataSourceBuilder.password(password);
        dataSourceBuilder.url(url);
        final DataSource dataSource = dataSourceBuilder.build();
        return dataSource;
    }

    @Bean
    public LobHandler lobHandler() {
        final DefaultLobHandler defaultLobHandler = new DefaultLobHandler();
        defaultLobHandler.setWrapAsLob(true);
        return defaultLobHandler;
    }

    @Autowired
    @Bean("jdbcTemplate")
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate;
    }

    @Autowired
    @Bean("jdbcTemplateDao")
    public TemplateDao jdbcCipDao(JdbcTemplate jdbcTemplate, LobHandler lobHandler) {
        final TemplateDao templateDao = new JdbcTemplateDao(jdbcTemplate, lobHandler);
        return templateDao;
    }
}
