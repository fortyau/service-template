package net.cyc.service.template.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import net.cyc.service.template.service.batch.TemplateServiceTasks;

/**
 * Template class for configuring scheduled batch processes
 * set your time intervals on when to run and the method
 * that you wish to run at that time.
 *
 */
@Configuration
@EnableScheduling
public class TemplateServiceTasksSchedulingConfigurer implements SchedulingConfigurer {

    private final long delayInMilliseconds = 60000; // 60s * 1000ms/s = 60000ms

    @Autowired
    private TemplateServiceTasks templateServiceTasks;

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        scheduledTaskRegistrar.addFixedDelayTask(
                () -> templateServiceTasks.callSomeMethod(),
                delayInMilliseconds
        );
    }
}
