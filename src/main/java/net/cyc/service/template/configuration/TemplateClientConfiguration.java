package net.cyc.service.template.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.cyc.service.template.client.TemplateClient;

@Configuration
public class TemplateClientConfiguration {

    @Value("${notification.url}")
    private String notificationUrl;

    @Value("${notification.endpoint}")
    private String notificationEndpoint;

    @Autowired
    @Bean("notificationClient")
    public TemplateClient templateClient() {
        final TemplateClient templateClient = new TemplateClient(notificationUrl, notificationEndpoint);
        return templateClient;
    }

}
