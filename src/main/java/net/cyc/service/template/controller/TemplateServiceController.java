package net.cyc.service.template.controller;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.cyc.service.template.domain.*;
import net.cyc.service.template.service.TemplateService;

import java.util.List;
import java.util.Objects;

/**
 * Controller class for handling CIP requests
 */
@RestController
@RequestMapping("/template")
public class TemplateServiceController {

    /**
     * The {@link TemplateService} used to process the requests.
     */
    private final TemplateService templateService;

    /**
     * Constructor.
     *
     * @throws NullPointerException if the given {@link TemplateService} is {@code null}.
     */
    @Autowired
    public TemplateServiceController(TemplateService templateService) {
        this.templateService = Objects.requireNonNull(templateService, "templateService cannot be null.");
    }


    /**
     * Controller method for handling requests for the status(es) of 0+ CIP orders.
     *
     * @param serviceRequest request object for the status(es) of 0+ CIP orders.
     * @return {@link CipRetrieveListResponse}.
     */
    @PostMapping(value = "/retrieve/list",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ServiceResponse objRetrieveList(@RequestBody ServiceRequest serviceRequest) {
        final List<Object> objOrderList = getTempalteService().someMethod(serviceRequest);
        final ServiceResponse serviceResponse = new ServiceResponse(true, "some message");
        return serviceResponse;
    }


    /**
     * @return templateService
     */
    private TemplateService getTempalteService() {
        return templateService;
    }
}
