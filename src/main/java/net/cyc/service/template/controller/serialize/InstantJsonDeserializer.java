package net.cyc.service.template.controller.serialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import net.cyc.service.template.domain.Constants;

import java.io.IOException;
import java.time.Instant;

/**
 * {@link JsonDeserializer} for {@link Instant}.
 */
public class InstantJsonDeserializer extends JsonDeserializer<Instant> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Instant deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return Instant.from(Constants.DATE_TIME_FORMATTER.parse(jsonParser.getText()));
    }
}
