package net.cyc.service.template.controller.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import net.cyc.service.template.domain.Constants;

import java.io.IOException;
import java.time.LocalDate;

public class LocalDateJsonSerializer extends JsonSerializer<LocalDate> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void serialize(LocalDate localDate, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        final String string = localDate.format(Constants.DATE_FORMATTER);
        jsonGenerator.writeString(string);
    }
}
