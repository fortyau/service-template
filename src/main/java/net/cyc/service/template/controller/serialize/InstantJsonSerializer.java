package net.cyc.service.template.controller.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import net.cyc.service.template.domain.Constants;

import java.io.IOException;
import java.time.Instant;

/**
 * {@link JsonSerializer} for {@link Instant}.
 */
public class InstantJsonSerializer extends JsonSerializer<Instant> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void serialize(Instant instant, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        final String string = Constants.DATE_TIME_FORMATTER.format(instant);
        jsonGenerator.writeString(string);
    }

}
