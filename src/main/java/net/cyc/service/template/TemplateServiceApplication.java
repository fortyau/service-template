package net.cyc.service.template;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class TemplateServiceApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(TemplateServiceApplication.class);

	public static void main(String[] args) {
		try {
			SpringApplication.run(TemplateServiceApplication.class, args);
		} catch (Exception exception) {
			LOGGER.error("Failed to start application. Exception thrown: ", exception);
			System.exit(1);
		}
	}
}
