INSERT INTO cip_orders
  (id, cip_status, status_modified, created, modified,
   employer_id, employee_id, first_name, last_name, ssn, dob,
   street_address_1, street_address_2, city, state, zipcode, phone, email, batch_id, record_id, reasons)
VALUES
  (1001, 'PAS', '2017-01-02 03:04:05', '2018-02-03 04:05:06', '2019-03-04 05:06:07',
   1, 1, 'alice', 'smith', '111223333', '1912-01-02',
   '111 1st avenue', 'apartment 1a', 'seattle', 'WA', '12345', '1113335555', 'asmith@fake.com',
    1, 1, null),
  (1002, 'UNP', '2020-02-03 04:05:06', '2021-03-04 05:06:07', '2022-04-05 06:07:08',
   1, 2, 'bob', 'jones', '222334444', '1923-03-04',
   '222 2nd street', 'apt 2b', 'nashville', 'TN', '23456', '3335557777', 'bjones@fake.com',
   2, 1, null),
  (1003, 'DEC', '2023-03-04 05:06:07', '2024-04-05 06:07:08', '2025-05-06 07:08:09',
   1, 3, 'chris', 'davis', '333445555', '1934-05-06',
   '333 3rd road', 'suite 3c', 'charlotte', 'NC', '34567', '5557779999', 'cdavis@fake.com',
   3, 1, null),
  (1004, 'PAS', '2026-04-05 06:07:08', '2027-05-06 07:08:09', '2028-06-07 08:09:10',
   2, 4, 'david', 'williams', '999887777', '1945-06-07',
   '444 4th boulevard', 'penthouse', 'rome', 'GA', '45678', '7779991111', 'dwilliams@fake.com',
   4, 1, null)
;
